#!/bin/bash

# ltnmp版本号
ltnmp_version='2.1.0'
ltnmp_tengine='tengine-2.1.1'
ltnmp_nginx='nginx-1.9.4'
ltnmp_php='php-5.6.12'
ltnmp_phpmyadmin='phpMyAdmin-4.4.14-all-languages'
ltnmp_mariadb='mariadb-10.0.21'
ltnmp_mysql='mysql-5.6.26'

# 附加模块版本
ltnmp_swoole='swoole-1.7.19'
ltnmp_phalcon='phalcon-v2.0.7'
ltnmp_redis='redis-3.0.3'
ltnmp_php_redis='redis-2.2.7'
ltnmp_yaf='yaf-2.3.4'

# 系统组件
ltnmp_autoconf='autoconf-2.13'
ltnmp_curl='curl-7.42.1'
ltnmp_freetype='freetype-2.6'
ltnmp_jemalloc='jemalloc-3.6.0'
ltnmp_libiconv='libiconv-1.14'
ltnmp_libiconv_glibc='libiconv-glibc-2.16'
ltnmp_libmcrypt='libmcrypt-2.5.8'
ltnmp_mhash='mhash-0.9.9.9'
ltnmp_mcrypt='mcrypt-2.6.8'
ltnmp_pcre='pcre-8.36'
ltnmp_cmake='cmake-3.3.1'